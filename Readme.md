# GNST Innovation Days

ElasticSearch + Logstash + Kibana, this is the fanciest description that I have ever made :)

## Getting started

### Set up Docker

1. Visit https://www.docker.com/community-edition and download latest stable release.
2. Install Docker CE

    Verify installed Docker version by executing:
    ```
    docker --version
    ```
    
    Verify the version of installed Docker Compose by executing:
    ```
    docker-compose --version
    ```
    In case docker-compose is not installed on your system, visit https://docs.docker.com/compose/install/

3. Assign at least 4 GB RAM to Docker

    - Windows: This can be done from Docker Settings or by executing following 
    
    - Linux/Mac: Visit https://docs.docker.com/engine/reference/commandline/info/ for more information

    Verify the settings by executing following command in powershell:

    ```
    docker info | Select-String  "^CPUs|^Total"
    ```
## Set up ELK stack
### Configure the network settings

Change the port numbers in docker-compose.yml if they are already in use. Otherwise basic configuration is fine.
```
ports:
  - "EXPOSED_PORT_FOR_KIBANA:5601"
  - "EXPOSED_PORT_FOR_ELASTICSEARCH:9200"
  - "EXPOSED_PORT_FOR_LOGSTASH:5044"
```

    TODO: Update the networking part

### Configure ELK

You can edit any of configration files located in .\opt and .\etc directories. Here you can find a pattern:
```
    SOURCE_DIRECTORY_ON_HOST:DESTINATION_DIRECTORY_ON_CONTAINER
```
And the example configuration.
```
volumes:
  - .\opt\kibana:/opt/kibana/config
  - .\opt\logstash:/opt/logstash/config
  - .\opt\elasticsearch:/opt/elasticsearch/config
  - .\etc\logstash:/etc/logstash/conf.d
  - .\etc\elasticsearch:/etc/elasticsearch
```
#### ElasticSearch configuration
    TODO: Update this

### Kibana configuration
    TODO: Update this

#### Logstash configuration
##### File Input plugin
https://www.elastic.co/guide/en/logstash/current/plugins-inputs-file.html
    
    TODO: Update this



